Run the following to compile the hello world application:
```
gcc -framework Foundation main.m -o hello-world
```

Authors:
James Ridgway
Linus Cash

Website:
http://www.bit-spark.com
